#_Angelo d'Elia_
## CsCodingChallenge
### Build
`mvn clean install`

### Run
`java -jar target/CsCodingChallenge-1.0.jar itunes.json`

### Notes:

- This version is based to run into the target directory. 
Dependencies are added into the `lib` subfolder and the relative path between jar file and lib folder must be preserved.
This has been preferred to create a `jar-with-dependencies`
- Parameter may be omitted, it defaults to `-/itunes.json`

