package info.angelodelia.cscodechallenge.analysys;

import info.angelodelia.cscodechallenge.model.ResultItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class TotalTrackTimeAnalysysTest {
  
  private TotalTrackTimeAnalysys sut;
  
  @BeforeEach
  void init() {
    this.sut = new TotalTrackTimeAnalysys();
  }
  
  @Test
  void shouldCalculateTotalTrackTime() {
    // given
    final List<ResultItem> items = new ArrayList<>();
    
    final int mins = 1;
    final ResultItem item1 = new ResultItem();
    item1.setTrackTimeMillis(mins * 60 * 1000L);
    items.add(item1);
    
    final int hours = 20;
    final ResultItem item2 = new ResultItem();
    item2.setTrackTimeMillis(hours * 60 * 60 * 1000L);//20 h
    items.add(item2);
    
    final int secs = 30;
    final ResultItem item3 = new ResultItem();
    item3.setTrackTimeMillis(secs * 1000L);
    items.add(item3);
    
    
    // when
    final String result = this.sut.execute(items);
    
    // then
    assertThat(result).isNotNull();
    assertThat(result).isNotEmpty();
    assertThat(result).contains(String.format("Total track time: %d hours, %d minutes, %d seconds", hours, mins, secs));
    
  }
  
}
