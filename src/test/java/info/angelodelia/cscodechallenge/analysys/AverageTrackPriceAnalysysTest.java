package info.angelodelia.cscodechallenge.analysys;

import info.angelodelia.cscodechallenge.model.ResultItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class AverageTrackPriceAnalysysTest {
  private AverageTrackPriceAnalysys sut;
  
  @BeforeEach
  void init() {
    this.sut = new AverageTrackPriceAnalysys();
  }
  
  @Test
  void shouldCalculateAveragePrice() {
    // given
    final List<ResultItem> items = new ArrayList<>();
    final BigDecimal price1 = new BigDecimal(1.0d);
    final BigDecimal price2 = new BigDecimal(2.0d);
    final BigDecimal price3 = new BigDecimal(3.0d);
    final BigDecimal average = price1.add(price2).add(price3).divide(new BigDecimal(3), RoundingMode.HALF_DOWN);
    
    final ResultItem item1 = new ResultItem();
    item1.setTrackPrice(price1);
    items.add(item1);
    
    final ResultItem item2 = new ResultItem();
    item2.setTrackPrice(price2);
    items.add(item2);
    
    final ResultItem item3 = new ResultItem();
    item3.setTrackPrice(price3);
    items.add(item3);
    
    
    // when
    final String result = this.sut.execute(items);
    
    // then
    assertThat(result).isNotNull();
    assertThat(result).isNotEmpty();
    assertThat(result).contains(String.format("Average price of a track %5.2f", average));
    
  }
  
}
