package info.angelodelia.cscodechallenge.analysys;

import info.angelodelia.cscodechallenge.model.ResultItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class NumberOfCollectionAnalysysTest {
  
  private NumberOfCollectionAnalysys sut;
  
  @BeforeEach
  void init() {
    this.sut = new NumberOfCollectionAnalysys();
  }
  
  @Test
  void shouldCalculateNumberOfCollections() {
    // given
    final int collections = 2;
    final long collectionId1 = 666;
    final long collectionId2 = 999;
    final List<ResultItem> items = new ArrayList<>();
    
    final ResultItem item1 = new ResultItem();
    item1.setCollectionId(collectionId1);
    items.add(item1);
    
    final ResultItem item2 = new ResultItem();
    item2.setCollectionId(collectionId1);
    items.add(item2);
    
    final ResultItem item3 = new ResultItem();
    item3.setCollectionId(collectionId2);
    items.add(item3);
    
    
    // when
    final String result = this.sut.execute(items);
    
    // then
    assertThat(result).isNotNull();
    assertThat(result).isNotEmpty();
    assertThat(result).contains(String.format("Number of different collections: %d", collections));
    
  }
}
