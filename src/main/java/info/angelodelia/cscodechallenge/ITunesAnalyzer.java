package info.angelodelia.cscodechallenge;

import info.angelodelia.cscodechallenge.analysys.ITunesAnalysys;
import info.angelodelia.cscodechallenge.datasource.ITunesDataSource;
import info.angelodelia.cscodechallenge.model.ResultItem;
import info.angelodelia.cscodechallenge.model.SearchResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by IntelliJ IDEA.
 * User: angelo
 * Date: 21/11/18
 * Time: 19.35
 * SVN Id: $Id:$
 * .
 */
@Slf4j
@RequiredArgsConstructor
public class ITunesAnalyzer {

  private final Set<ITunesAnalysys> analyzers;
  
  
  public Set<String> run(final ITunesDataSource dataSource) {
    // get the data
    final SearchResult results = dataSource.getSearchResult();

    // just to check...
    if (results.getResultCount() != results.getResults().size()) {
      log.warn("Declared {} results, parsed {}", results.getResultCount(), results.getResults().size());
    }


    // remove all the !"song"
    final List<ResultItem> filteredItems = results.getResults().stream()
      .filter(resultItem -> "song".equalsIgnoreCase(resultItem.getKind()))
      .collect(Collectors.toList());
    log.trace("filtered {} songs", filteredItems.size());

    // execute the analysys
    final Set<String> analysysResults = this.analyzers.stream()
      .map(iTunesAnalysys -> iTunesAnalysys.execute(filteredItems))
      .collect(Collectors.toSet());
    
    return analysysResults;
  }

}
