package info.angelodelia.cscodechallenge;

import info.angelodelia.cscodechallenge.analysys.ITunesAnalysys;
import info.angelodelia.cscodechallenge.datasource.FileDataSource;
import info.angelodelia.cscodechallenge.datasource.ITunesDataSource;
import lombok.extern.slf4j.Slf4j;
import org.reflections.Reflections;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by IntelliJ IDEA.
 * User: angelo
 * Date: 21/11/18
 * Time: 19.33
 * SVN Id: $Id:$
 * .
 */
@Slf4j
public class Application {

  public static void main(final String[] args) {

    String fileName = "itunes.json";
    if (args.length >= 1 && args[0] != null && !args[0].isEmpty()) {
      fileName = args[0];
    }

    final ITunesDataSource dataSource = new FileDataSource(fileName);
    // look for all the analyzers
    final Set<ITunesAnalysys> analysysList = lookupAllAnalysys();
  
    final ITunesAnalyzer analyzer = new ITunesAnalyzer(analysysList);
    try {
      final Set<String> analysysResults = analyzer.run(dataSource);
      // print the results
      analysysResults.forEach(x -> System.out.println("Analysys: " + x));
  
    } catch (final Exception ex) {
      log.error("An Error Occoured", ex);
      System.err.println(ex.getMessage());
    }

  }
  
  private static Set<ITunesAnalysys> lookupAllAnalysys() {
    final Reflections reflections = new Reflections("info.angelodelia.cscodechallenge");
    final Set<Class<? extends ITunesAnalysys>> analyzerClasses = reflections.getSubTypesOf(ITunesAnalysys.class);
    log.trace("Found {}", analyzerClasses.size());
    
    final Set<ITunesAnalysys> analyzers = analyzerClasses.stream()
      .map(Application::getInstance)
      .filter(Objects::nonNull)
      .collect(Collectors.toSet());
    log.trace("Returning {}", analyzers);
    
    return analyzers;
  }
  
  private static ITunesAnalysys getInstance(final Class<? extends ITunesAnalysys> className) {
    try {
      return className.newInstance();
    } catch (final Exception e) {
      log.error("Unable to instantiate {}", className.getName());
      return null;
    }
  }
  
}
