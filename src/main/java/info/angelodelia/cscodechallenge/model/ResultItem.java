package info.angelodelia.cscodechallenge.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by IntelliJ IDEA.
 * User: angelo
 * Date: 21/11/18
 * Time: 19.41
 * SVN Id: $Id:$
 * .
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResultItem {


  private Long trackId;

  private Long collectionId;

  private String kind;

  private Long trackTimeMillis;
  
  private BigDecimal trackPrice;


}
