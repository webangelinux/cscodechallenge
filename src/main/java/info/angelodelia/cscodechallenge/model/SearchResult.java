package info.angelodelia.cscodechallenge.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: angelo
 * Date: 21/11/18
 * Time: 19.37
 * SVN Id: $Id:$
 * .
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchResult {

  private int resultCount;

  private List<ResultItem> results;

}
