package info.angelodelia.cscodechallenge.analysys;

import info.angelodelia.cscodechallenge.model.ResultItem;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: angelo
 * Date: 21/11/18
 * Time: 21.14
 * SVN Id: $Id:$
 * .
 */
@Slf4j
public class TotalTrackTimeAnalysys implements ITunesAnalysys {

  @Override
  public String execute(final List<ResultItem> items) {

    final Long totalMillis = items.stream()
      .mapToLong(ResultItem::getTrackTimeMillis)
      .sum();

    log.trace("total time mills {}", totalMillis);

    final int seconds = (int) (totalMillis / 1000) % 60;
    final int minutes = (int) ((totalMillis / (1000 * 60)) % 60);
    final int hours = (int) ((totalMillis / (1000 * 60 * 60)) % 24);

    return String.format("Total track time: %d hours, %d minutes, %d seconds", hours, minutes, seconds);
  }
}
