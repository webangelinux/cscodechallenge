package info.angelodelia.cscodechallenge.analysys;

import info.angelodelia.cscodechallenge.model.ResultItem;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Objects;

public class AverageTrackPriceAnalysys implements ITunesAnalysys {
  @Override
  public String execute(final List<ResultItem> items) {
    
    final BigDecimal[] totalWithCount = items.stream()
      .map(ResultItem::getTrackPrice)
      .filter(Objects::nonNull)
      .map(bd -> new BigDecimal[]{bd, BigDecimal.ONE})
      .reduce((a, b) -> new BigDecimal[]{a[0].add(b[0]), a[1].add(BigDecimal.ONE)})
      .get();
    
    final BigDecimal averagePrice = totalWithCount[0].divide(totalWithCount[1], RoundingMode.HALF_DOWN);
    
    return String.format("Average price of a track %5.2f", averagePrice);
  }
  //  Average price of a track <price >
  
}
