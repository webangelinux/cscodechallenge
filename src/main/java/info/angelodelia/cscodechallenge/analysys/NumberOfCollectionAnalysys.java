package info.angelodelia.cscodechallenge.analysys;

import info.angelodelia.cscodechallenge.model.ResultItem;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class NumberOfCollectionAnalysys implements ITunesAnalysys {
  
  //  Number of different collections: <number of distinct collections>
  
  @Override
  public String execute(final List<ResultItem> items) {
    
    final long numberOfCollections = items.stream()
      .map(ResultItem::getCollectionId)
      .distinct().count();
    
    log.trace("found {}", numberOfCollections);
    
    return String.format("Number of different collections: %d", numberOfCollections);
  }
}
