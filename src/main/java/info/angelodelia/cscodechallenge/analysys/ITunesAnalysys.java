package info.angelodelia.cscodechallenge.analysys;

import info.angelodelia.cscodechallenge.model.ResultItem;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: angelo
 * Date: 21/11/18
 * Time: 20.49
 * SVN Id: $Id:$
 * .
 */
public interface ITunesAnalysys {

  String execute(List<ResultItem> items);

}
