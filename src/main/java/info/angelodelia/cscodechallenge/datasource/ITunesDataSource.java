package info.angelodelia.cscodechallenge.datasource;

import info.angelodelia.cscodechallenge.model.SearchResult;

/**
 * Created by IntelliJ IDEA.
 * User: angelo
 * Date: 21/11/18
 * Time: 19.36
 * SVN Id: $Id:$
 * .
 */
public interface ITunesDataSource {

  SearchResult getSearchResult();

}
