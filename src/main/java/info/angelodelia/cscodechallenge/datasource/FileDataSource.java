package info.angelodelia.cscodechallenge.datasource;

import com.fasterxml.jackson.databind.ObjectMapper;
import info.angelodelia.cscodechallenge.model.SearchResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by IntelliJ IDEA.
 * User: angelo
 * Date: 21/11/18
 * Time: 19.49
 * SVN Id: $Id:$
 * .
 */
@Slf4j
@RequiredArgsConstructor
public class FileDataSource implements ITunesDataSource {

  private final String filePath;


  @Override
  public SearchResult getSearchResult() {
    this.checkFileExists();
    log.trace("File {} found", this.filePath);

    //read json file data to String
    final byte[] jsonData;
    try {
      jsonData = Files.readAllBytes(Paths.get(this.filePath));

      final ObjectMapper objectMapper = new ObjectMapper();

      return objectMapper.readValue(jsonData, SearchResult.class);

    } catch (final IOException e) {
      throw new RuntimeException("File " + this.filePath + " cannot be read/parsed because of: " + e.getMessage(), e);
    }


  }

  private void checkFileExists() {
    final File file = new File(this.filePath);
    if (!file.exists()) {
      throw new RuntimeException("File " + this.filePath + " is not existing.");
    }
  }
}
